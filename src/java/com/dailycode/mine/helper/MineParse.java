/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dailycode.mine.helper;

import com.dailycode.mine.entity.Record;
import com.dailycode.mine.entity.Shift;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Kris Sadewo <krissadewo@ossys.com>
 */
public class MineParse {

    private static MineParse instance;

    private MineParse() {
    }

    public static MineParse getInstance() {
        if (instance == null) {
            instance = new MineParse();
        }
        return instance;
    }

    public Shift parseShiftData() {
        File file = new File("c:\\temp").listFiles()[0];
        System.out.println(file.getName());
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader br = null;
        List<Record> records = new ArrayList<Record>();
        try {
            String currentLine;
            br = new BufferedReader(new FileReader(file));
            int i = 0;
            int start = 1000;
            while ((currentLine = br.readLine()) != null) {
                stringBuilder.append(currentLine);
                if (currentLine.endsWith("q")) {
                    start = i;
                }
                if (i > start) {
                    Record record = new Record();
                    record.setShovel(StringUtils.split(currentLine, 'x')[0].trim());
                    record.setTruck(StringUtils.split(currentLine, 'x')[1].trim());
                    record.setTimeFull(StringUtils.split(currentLine, 'x')[2].trim());
                    record.setLoadLocation(StringUtils.split(currentLine, 'x')[3].trim());
                    record.setMaterialType(StringUtils.split(currentLine, 'x')[4].trim());
                    record.setDumpGrade(StringUtils.split(currentLine, 'x')[5].trim());
                    record.setHaulLocation(StringUtils.split(currentLine, 'x')[6].trim());
                    record.setBay(StringUtils.split(currentLine, 'x')[7].trim());
                    if (StringUtils.isNotEmpty(StringUtils.split(currentLine, 'x')[8].trim())) {
                        record.setMins(Integer.valueOf(StringUtils.split(currentLine, 'x')[8].trim()));
                    } else {
                        record.setMins(0);
                    }

                    if (StringUtils.isNotEmpty(StringUtils.split(currentLine, 'x')[9].trim())) {
                        record.setTonnes(Double.valueOf(StringUtils.split(currentLine, 'x')[9].trim()));
                    } else {
                        record.setTonnes(0.d);
                    }
                    record.setDel(StringUtils.split(currentLine, 'x')[10].trim());
                    record.setLoadOrBadDump(StringUtils.split(currentLine, 'x')[11].trim());
                    System.out.println(record.toString());
                    records.add(record);
                }
                i++;
            }


        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        StringBuilder header = new StringBuilder();
        for (char data : stringBuilder.toString().split("Shift: -")[1].toCharArray()) {
            if (data == '+') {
                break;
            }
            header.append(data);
        }

        try {
            Shift shift = new Shift();
            shift.setShift(StringUtils.split(header.toString(), ' ')[1] + " " + StringUtils.split(header.toString(), ' ')[2]);
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yy");
            shift.setLoadDate(dateFormat.parse(StringUtils.split(header.toString(), ' ')[0]));
            shift.setRecords(records);
            return shift;
        } catch (ParseException ex) {
            Logger.getLogger(MineParse.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }
}
