/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dailycode.mine.dao.impl;

import com.dailycode.mine.dao.RecordDao;
import com.dailycode.mine.entity.Record;
import com.dailycode.mine.entity.Shift;
import com.dailycode.mine.helper.DBConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kris Sadewo <krissadewo@ossys.com>
 */
public class RecordDaoImpl implements RecordDao {

    private Connection connection;

    public RecordDaoImpl() {
        try {
            connection = DBConnection.getInstance().getConnection();
        } catch (SQLException ex) {
            Logger.getLogger(RecordDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int save(Record entiy) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public int update(Record entity) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public int delete(Record entity) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List<Record> getAll() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Record getById(Record entity) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List<Record> getRecordByDateAndShiftType(String shiftType, Date startDate, Date endDate) {
        String sql = "SELECT *FROM tbrecord tr "
                + " INNER JOIN tbshift ts ON ts.id_shift = tr.id_shift"
                + " WHERE ts.shift= ?"
                + " AND ts.load_date BETWEEN ? AND ?";
        List<Record> records = new ArrayList<Record>();

        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, shiftType);
            ps.setDate(2, new java.sql.Date(startDate.getTime()));
            ps.setDate(3, new java.sql.Date(endDate.getTime()));
            ResultSet rs = ps.executeQuery();
            System.out.println(rs.getStatement());
            while (rs.next()) {
                Shift shift = new Shift();
                shift.setId(rs.getInt("id_shift"));
                shift.setShift(rs.getString("shift"));
                shift.setLoadDate(rs.getDate("load_date"));

                //Please adjust with field name on the db, I'm so tired to write all column....
                Record record = new Record();
                record.setShovel(rs.getString("shovel"));
                record.setTruck(rs.getString("truck"));
                record.setTimeFull(rs.getString("time_full"));
                record.setDumpGrade(rs.getString("dump_grade"));
                record.setMaterialType(rs.getString("material_type"));

                record.setShift(shift);
                records.add(record);
            }

        } catch (SQLException ex) {
        }
        return records;

    }
}
