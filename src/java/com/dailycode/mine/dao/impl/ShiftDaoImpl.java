/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dailycode.mine.dao.impl;

import com.dailycode.mine.dao.ShiftDao;
import com.dailycode.mine.entity.Record;
import com.dailycode.mine.entity.Shift;
import com.dailycode.mine.helper.DBConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kris Sadewo <krissadewo@ossys.com>
 */
public class ShiftDaoImpl implements ShiftDao {

    private Connection connection;

    public ShiftDaoImpl() {
        try {
            connection = DBConnection.getInstance().getConnection();
        } catch (SQLException ex) {
            Logger.getLogger(ShiftDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int save(Shift entiy) {
        String sqlShift = "INSERT INTO tbshift(shift,load_date) VALUES(?,?)";
        String sqlLastInsertId = "SELECT MAX(id_shift) AS id_shift FROM tbshift";
        String sqlRecord = "INSERT INTO tbrecord"
                + "(id_shift,shovel,truck,time_full,load_location,material_type,"
                + "dump_grade,haul_location,bay,mins,tonnes,del,load_or_bad_dump) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement ps1 = connection.prepareStatement(sqlShift);
            ps1.setString(1, entiy.getShift());
            ps1.setDate(2, new java.sql.Date(entiy.getLoadDate().getTime()));
            ps1.executeUpdate();

            int idShift = 0;
            PreparedStatement ps2 = connection.prepareStatement(sqlLastInsertId);
            ResultSet rs = ps2.executeQuery();
            while (rs.next()) {
                idShift = rs.getInt("id_shift");
            }

            PreparedStatement ps3 = connection.prepareStatement(sqlRecord);
            for (Record record : entiy.getRecords()) {
                synchronized (record) {
                    ps3.setInt(1, idShift);
                    ps3.setString(2, record.getShovel());
                    ps3.setString(3, record.getTruck());
                    ps3.setString(4, record.getTimeFull());
                    ps3.setString(5, record.getLoadLocation());
                    ps3.setString(6, record.getMaterialType());
                    ps3.setString(7, record.getDumpGrade());
                    ps3.setString(8, record.getHaulLocation());
                    ps3.setString(9, record.getBay());
                    ps3.setInt(10, record.getMins());
                    ps3.setDouble(11, record.getTonnes());
                    ps3.setString(12, record.getDel());
                    ps3.setString(13, record.getLoadOrBadDump());
                    ps3.executeUpdate();
                    System.out.println("saving : " + record.toString());
                }
            }
            return 1;
        } catch (SQLException ex) {
            Logger.getLogger(ShiftDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public int update(Shift entity) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public int delete(Shift entity) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List<Shift> getAll() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Shift getById(Shift entity) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
