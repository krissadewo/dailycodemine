/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dailycode.mine.dao;

import java.util.List;

/**
 *
 * @author Kris Sadewo <krissadewo@ossys.com>
 */
public interface BaseDao<T> {

    int save(T entiy);

    int update(T entity);

    int delete(T entity);

    List<T> getAll();

    T getById(T entity);
}
