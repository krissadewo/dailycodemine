/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dailycode.mine.dao;

import com.dailycode.mine.entity.Record;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Kris Sadewo <krissadewo@ossys.com>
 */
public interface RecordDao extends BaseDao<Record> {

    List<Record> getRecordByDateAndShiftType(String shiftType, Date startDate, Date endDate);
}
