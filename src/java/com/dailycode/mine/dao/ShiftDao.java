/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dailycode.mine.dao;

import com.dailycode.mine.entity.Shift;

/**
 *
 * @author Kris Sadewo <krissadewo@ossys.com>
 */
public interface ShiftDao extends BaseDao<Shift> {
}
