/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dailycode.mine.entity;

import java.util.Date;
import java.util.List;

/**
 *
 * @author Kris Sadewo <krissadewo@ossys.com>
 */
public class Shift {

    private Integer id;
    private String shift;
    private Date loadDate;
    //One To Many
    private List<Record> records;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getShift() {
        return shift;
    }

    public void setShift(String shift) {
        this.shift = shift;
    }

    public Date getLoadDate() {
        return loadDate;
    }

    public void setLoadDate(Date loadDate) {
        this.loadDate = loadDate;
    }

    public List<Record> getRecords() {
        return records;
    }

    public void setRecords(List<Record> records) {
        this.records = records;
    }

    @Override
    public String toString() {
        return "Shift{" + "id=" + id + ", shift=" + shift + ", loadDate=" + loadDate + ", records=" + records + '}';
    }
}
