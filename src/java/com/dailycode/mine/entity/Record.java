/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dailycode.mine.entity;

/**
 *
 * @author Kris Sadewo <krissadewo@ossys.com>
 */
public class Record {

    private Integer id;
    private String shovel;
    private String truck;
    private String timeFull;
    private String loadLocation;
    private String materialType;
    private String dumpGrade;
    private String haulLocation;
    private String bay;
    private Integer mins;
    private Double tonnes;
    private String del;
    private String loadOrBadDump;
    //Many To One
    private Shift shift;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getShovel() {
        return shovel;
    }

    public void setShovel(String shovel) {
        this.shovel = shovel;
    }

    public String getTruck() {
        return truck;
    }

    public void setTruck(String truck) {
        this.truck = truck;
    }

    public String getTimeFull() {
        return timeFull;
    }

    public void setTimeFull(String timeFull) {
        this.timeFull = timeFull;
    }

    public String getLoadLocation() {
        return loadLocation;
    }

    public void setLoadLocation(String loadLocation) {
        this.loadLocation = loadLocation;
    }

    public String getMaterialType() {
        return materialType;
    }

    public void setMaterialType(String materialType) {
        this.materialType = materialType;
    }

    public String getDumpGrade() {
        return dumpGrade;
    }

    public void setDumpGrade(String dumpGrade) {
        this.dumpGrade = dumpGrade;
    }

    public String getHaulLocation() {
        return haulLocation;
    }

    public void setHaulLocation(String haulLocation) {
        this.haulLocation = haulLocation;
    }

    public String getBay() {
        return bay;
    }

    public void setBay(String bay) {
        this.bay = bay;
    }

    public Integer getMins() {
        return mins;
    }

    public void setMins(Integer mins) {
        this.mins = mins;
    }

    public Double getTonnes() {
        return tonnes;
    }

    public void setTonnes(Double tonnes) {
        this.tonnes = tonnes;
    }

    public String getDel() {
        return del;
    }

    public void setDel(String del) {
        this.del = del;
    }

    public Shift getShift() {
        return shift;
    }

    public void setShift(Shift shift) {
        this.shift = shift;
    }

    public String getLoadOrBadDump() {
        return loadOrBadDump;
    }

    public void setLoadOrBadDump(String loadOrBadDump) {
        this.loadOrBadDump = loadOrBadDump;
    }

    @Override
    public String toString() {
        return "Record{" + "id=" + id + ", shovel=" + shovel + ", truck=" + truck + ", timeFull=" + timeFull + ", loadLocation=" + loadLocation + ", materialType=" + materialType + ", dumpGrade=" + dumpGrade + ", haulLocation=" + haulLocation + ", bay=" + bay + ", mins=" + mins + ", tonnes=" + tonnes + ", del=" + del + ", loadOrBadDump=" + loadOrBadDump + ", shift=" + shift + '}';
    }
}
