/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dailycode.mine.controller;

import com.dailycode.mine.dao.RecordDao;
import com.dailycode.mine.dao.impl.RecordDaoImpl;
import com.dailycode.mine.dao.impl.ShiftDaoImpl;
import com.dailycode.mine.entity.Record;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Kris Sadewo <krissadewo@ossys.com>
 */
public class ShiftReportController extends MineController {

    private RecordDao dao;

    public ShiftReportController() {
        dao = new RecordDaoImpl();
    }

    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher rd = request.getRequestDispatcher(PAGE_SHIFT_REPORT);
        rd.forward(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String shiftType = request.getParameter("shift");
        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");


        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
            List<Record> records = dao.getRecordByDateAndShiftType(shiftType, dateFormat.parse(startDate), dateFormat.parse(endDate));
            System.out.println(records.size());

            request.setAttribute("records", records);
            RequestDispatcher rd = request.getRequestDispatcher(PAGE_SHIFT_REPORT);
            rd.forward(request, response);
        } catch (ParseException ex) {
            Logger.getLogger(ShiftDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
