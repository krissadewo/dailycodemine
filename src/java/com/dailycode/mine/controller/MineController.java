/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dailycode.mine.controller;

import javax.servlet.http.HttpServlet;

/**
 *
 * @author Kris Sadewo <krissadewo@ossys.com>
 */
public class MineController extends HttpServlet {

    public static final String PAGE_SHIFT_REPORT = "/shift/shift_report.jsp";
    public static final String PAGE_UPLOAD = "/upload/upload.jsp";
}
