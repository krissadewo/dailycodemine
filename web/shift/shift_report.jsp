<%-- 
    Document   : index
    Created on : Dec 9, 2012, 12:13:01 PM
    Author     : Kris Sadewo <krissadewo@ossys.com>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script src="http://code.jquery.com/jquery-1.8.3.js"></script>
        <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>  
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css" />
        <!-- see http://jqueryui.com/datepicker/ -->
        <script>
            $(function() {
                $( "#startDate" ).datepicker();
            });
            
            $(function() {
                $( "#endDate" ).datepicker();
            });
        </script>
    </head>
    <body>
        <c:url value="/shift" var="shift"/>
        <form action="${shift}" method="post">
            <table>
                <tr>
                    <td>
                        Shift 
                    </td>
                    <td>
                        <select name="shift">          
                            <option value="All Shift" >All Shift</option>
                            <option value="Day Shift" selected="true" >Day Shift</option>
                            <option value="Night Shift">Night Shift</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        Start Date 
                    </td>
                    <td>
                        <input type="text" id="startDate" name="startDate" />
                    </td>
                </tr>
                <tr>
                    <td>
                        End Date 
                    </td>
                    <td>
                        <input type="text" id="endDate" name="endDate"/>
                    </td>
                </tr>
            </table>
            <input type="submit" value="Search"/>
        </form>

            <table border="1">
            <tr>
                <th>
                    Shift
                </th>
                <th>
                    Loaded Date
                </th>
                <th>
                    Shovel
                </th>
                <th>
                    Truck
                </th>
                <th>
                    Time Full
                </th>
                <th>
                    Dump Grade
                </th>
                <th>
                    Material Type
                </th>
            </tr>

            <c:forEach items="${records}" var="record">
                <tr>
                    <td><c:out value="${record.shift.shift}"/></td>
                    <td><c:out value="${record.shift.loadDate}"/></td>
                    <td><c:out value="${record.shovel}"/></td>
                    <td><c:out value="${record.truck}"/></td>
                    <td><c:out value="${record.timeFull}"/></td>
                    <td><c:out value="${record.dumpGrade}"/></td>
                    <td><c:out value="${record.materialType}"/></td>
                </tr>
            </c:forEach>
        </table>

    </body>
</html>
