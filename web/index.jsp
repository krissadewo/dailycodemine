<%-- 
    Document   : index
    Created on : Dec 9, 2012, 12:13:01 PM
    Author     : Kris Sadewo <krissadewo@ossys.com>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>     
    </head>
    <body>
        <h1>Hello World!</h1>
        <c:url value="/shift" var="shiftReport"/>
        <c:url value="/upload" var="upload"/>
        <a href="${shiftReport}">Shift </a><br>
        <a href="${upload}">Upload </a>
    </body>
</html>
